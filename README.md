# telegram-downloader

A Python program to download documents from telegram links

# Where to get API values

[Where to get API values](https://core.telegram.org/api/obtaining_api_id)

# Usage

It just read a link or a file that contains links and starts to download it, without limits

```
$> python main.py -h
usage: main.py [-h] [-c C] [-o O] [-l L] [-L L]

Python program to download documents from channels

optional arguments:
  -h, --help  show this help message and exit``
  -c C        alternate config file
  -o O        destination directory
  -l L        read telegram link
  -L L        read telegram links from a file
```



