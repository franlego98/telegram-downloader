import configparser
import argparse
from telethon.sync import TelegramClient
from telethon.tl.types import InputMessagesFilterDocument
import os,sys

def bytes_to_label(num):
	if num < 1024:
		return str(round(num)) + "B"
	elif num < 1024**2:
		return str(round(num/1024)) + "kB"
	elif num < 1024**3:
		return str(round(num/(1024**2))) + "MB"
	else:
		return str(round(num/(1024**3))) + "GB"

def progress(current,total):
	print("Downloaded: " + bytes_to_label(current) + " Total: " + bytes_to_label(total) + " | " + str(round(current*100/total,3)) + "%",flush=True,end="\r")

def downloaded(carpeta,fichero):
	for archivo in os.listdir(carpeta):
		if archivo == fichero:
			return True
		
	return False

def download_channel_link(tg_client,link,folder):
	channel_dir = folder + "/" + link.split("/")[-1] + "/"
	
	if not os.path.exists(channel_dir):
		os.mkdir(channel_dir)
	
	for msg in tg_client.iter_messages(link,limit = 100,filter = InputMessagesFilterDocument):
		nombre = msg.document.attributes[0].file_name
		if downloaded(channel_dir,nombre):
			print(nombre + " exists")
			if os.path.getsize(channel_dir+nombre) != msg.document.size:
				print("Sizes differs, redownloading")
				msg.download_media(file = channel_dir + nombre,progress_callback = progress)
		else:
			print(nombre + " downloading...")
			
			msg.download_media(file = channel_dir,progress_callback = progress)

def main():
	#read the arguments
	parser = argparse.ArgumentParser(description="Python program to download documents from channels")
	parser.add_argument('-c',help="alternate config file",default="telegram-downloader.ini")
	parser.add_argument('-o',help="destination directory",default="downloads")
	parser.add_argument('-l',help="read telegram link")
	parser.add_argument('-L',help="read telegram links from a file")
	
	args = parser.parse_args()
	
	
	#read config file
	config = configparser.ConfigParser()
	try:
		config.read(args.c)
	except:
		print("Error when reading config file -> ",args.c)
		
	tg_client = None
		
	#config parsed
	if "main" in config:
		main = config["main"]
		if "api_id" in main and "api_hash" in main:
			api_id = main["api_id"]
			api_hash = main["api_hash"]
			
			tg_client = TelegramClient('telegram-downloader', api_id, api_hash).start();
			
		else:
			print("Not defined API values")
				
	else:
		print("Not found API settings section")
	
	if tg_client is None:
		print("Failed to set up client")
		return
	
	if not os.path.exists(args.o):
		os.mkdir(args.o)
	
	if args.l is not None:
		print("Downloading",args.l,"to",args.o)
		download_channel_link(tg_client,args.l,args.o)
		
	if args.L is not None:
		print("Downloading from file link list:",args.L)
		with open(args.L) as f:
			for link in read():
				download_channel_link(tg_client,link,folder)
		
	
	
	
main()
